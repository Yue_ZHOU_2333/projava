package edu.uchicago.gerber._01control;

import java.util.Scanner;

public class E2_4 {
    public static void main(String[] args) {
        System.out.print("Please enter two integers: ");
        Scanner in = new Scanner(System.in);
        int a = in.nextInt();
        int b = in.nextInt();
        System.out.printf("The sum: %d %n", a + b);
        System.out.printf("The difference: %d %n", a - b);
        System.out.printf("The product: %d %n", a * b);
        System.out.printf("The average: %.2f %n", (a + b)/2.0);
        System.out.printf("The distance: %d %n", Math.abs(a - b));
        System.out.printf("The maximum: %d %n", Math.max(a, b));
        System.out.printf("The minimum: %d %n", Math.min(a, b));
    }
}
