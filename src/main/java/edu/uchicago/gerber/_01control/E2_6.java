package edu.uchicago.gerber._01control;

import java.util.Scanner;

public class E2_6 {
    public static void main(String[] args) {
        System.out.print("Please enter a measurement in meters: ");
        Scanner in = new Scanner(System.in);
        int meter = in.nextInt();
        final double METERSTOFEET = 1/0.3048;
        final double METERSTOMILES = 1/1609.0;
        final double METERSTOINCHES = 100/2.54;
        System.out.printf("converts it to miles: %.9f %n", meter * METERSTOMILES);
        System.out.printf("converts it to feet: %.9f %n", meter * METERSTOFEET);
        System.out.printf("converts it to inches: %.9f %n", meter * METERSTOINCHES);

    }
}
