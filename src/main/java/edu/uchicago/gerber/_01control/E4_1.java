package edu.uchicago.gerber._01control;

import java.util.Scanner;

import static java.lang.Math.*;

public class E4_1 {
    public static void main(String[] args) {
        int sum = 0;
        for (int i = 2; i <= 100; i = i + 2){
            sum = sum + i;
        }
        System.out.printf("The sum of all even number between 2 and 100 is: %d %n", sum);
        sum = 0;
        int i = 1;
        while(i<=10){
            sum = sum + i * i;
            i++;
        }
        System.out.printf("The sum of all squares between 1 and 100 is: %d %n", sum);
        System.out.printf("All powers of 2 from 2^0 up to 2^20: ");
        for (int j = 0; j <= 20; j++){
            System.out.print(pow(2,j));
            System.out.printf(" ");
        }
        System.out.printf(" %n");

        System.out.print("Please enter the a and b: ");
        Scanner in = new Scanner(System.in);
        int a = in.nextInt();
        int b = in.nextInt();
        int c = 0;
        if(a % 2 ==1){
            c = a;
        }
        else{
            c = a + 1;
        }
        sum = 0;
        while(c <= b){
            sum = sum + c;
            c = c + 2;
        }
        System.out.printf("The sum of all odd number between %d and %d is: %d %n", a, b, sum);

        System.out.print("Please enter the number: ");
        int temp = in.nextInt();
        sum = 0;
        while (temp > 0){
            if((temp % 10) % 2 == 1){
                sum = sum + temp % 10;
            }
            temp = temp / 10;
        }
        System.out.printf("The sum of odd digits is: %d %n", sum);
    }
}
