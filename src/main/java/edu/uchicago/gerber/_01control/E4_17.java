package edu.uchicago.gerber._01control;

import java.util.Scanner;

public class E4_17 {
    public static void main(String[] args) {
        System.out.print("Please enter the number: ");
        Scanner in = new Scanner(System.in);
        int num = in.nextInt();
        while (num > 0){
            System.out.printf("%d %n", num % 2);
            num = num / 2;
        }
    }
}
