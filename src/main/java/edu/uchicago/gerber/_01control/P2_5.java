package edu.uchicago.gerber._01control;

import java.util.Scanner;

public class P2_5 {
    public static void main(String[] args) {
        System.out.print("Please enter the price: ");
        Scanner in = new Scanner(System.in);
        double price = in.nextDouble();
        int dollars = (int)price;
        int cents = (int)((price - dollars) * 100 + 0.5);

        System.out.printf("The dollars: %d %n", dollars);
        System.out.printf("The cents: %d %n", cents);

    }
}
