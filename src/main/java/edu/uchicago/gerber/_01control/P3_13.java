package edu.uchicago.gerber._01control;

import java.util.Scanner;

public class P3_13 {
    public static void main(String[] args) {
        System.out.print("Please enter the number: ");
        Scanner in = new Scanner(System.in);
        int number = in.nextInt();
        String s = "";
        while (number != 0){
            if (number>=1000){
                s = s + "M";
                number -= 1000;
            }
            else if (number >= 900){
                s = s + "CM";
                number -= 900;
            }
            else if (number >= 500){
                s = s + "D";
                number -= 500;
            }
            else if (number >= 400){
                s = s + "CD";
                number -= 400;
            }
            else if (number >= 100){
                s = s + "C";
                number -= 100;
            }
            else if (number >= 90){
                s = s + "XC";
                number -= 90;
            }
            else if (number >= 50){
                s = s + "L";
                number -= 50;
            }
            else if (number >= 40){
                s = s + "XL";
                number -= 40;
            }
            else if (number >= 10){
                s = s + "X";
                number -= 10;
            }
            else if (number >= 9){
                s = s + "IX";
                number -= 9;
            }
            else if (number >= 5){
                s = s + "V";
                number -= 5;
            }
            else if (number >= 4){
                s = s + "IV";
                number -= 4;
            }
            else if (number >= 1){
                s = s + "I";
                number -= 1;
            }
        }

        System.out.println("The season is:" + s);
    }
}
