package edu.uchicago.gerber._01control;

import java.util.Scanner;

public class P3_14 {
    public static void main(String[] args) {
        System.out.print("Please enter the year: ");
        Scanner in = new Scanner(System.in);
        int year = in.nextInt();
        Boolean leapYear = true;
        if (year % 4 != 0 || (year % 100 == 0 && year % 400 != 0)) {
            leapYear = false;
        }


        System.out.println("The leapyear is:" + leapYear);
    }
}
