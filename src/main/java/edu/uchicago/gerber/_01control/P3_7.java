package edu.uchicago.gerber._01control;

import java.util.Scanner;

public class P3_7 {
    public static void main(String[] args) {
        System.out.print("Please enter the income: ");
        Scanner in = new Scanner(System.in);
        Double income = in.nextDouble();
        Double tax = 0.0;
        if (income <= 50000) {
            tax = 0.01 * income;
        }
        else if (income <= 75000 && income >= 50000){
            tax = 0.01 * 50000 + (income - 50000) * 0.02;
        }
        else if (income <= 100000 && income >= 75000){
            tax = 0.01 * 50000 + (75000 - 50000) * 0.02 + (income - 75000) * 0.03;
        }
        else if (income <= 250000 && income >= 100000){
            tax = 0.01 * 50000 + (75000 - 50000) * 0.02 + (10000 - 75000) * 0.03 + (income - 100000) * 0.04;
        }
        else if (income <= 500000 && income >= 250000) {
            tax = 0.01 * 50000 + (75000 - 50000) * 0.02 + (10000 - 75000) * 0.03 + (250000 - 100000) * 0.04 + (income - 250000) * 0.05;
        }
        else{
            tax = 0.01 * 50000 + (75000 - 50000) * 0.02 + (10000 - 75000) * 0.03 + (250000 - 100000) * 0.04 + (500000 - 250000) * 0.05 + (income - 500000) * 0.06;
        }
        System.out.println("The income tax is:" + tax);
    }
}
