package edu.uchicago.gerber._02arrays;

import java.util.Random;

public class E6_1 {
    public static void main(String[] args) {
        int[] array = new int[10];
        int max = 10;
        int min = 0;
        Random r = new Random();
        for(int i = 0;i < array.length;i++){
            array[i] = r.nextInt(max - min + 1) + min;
        }
        for(int i = 0;i < 10; i = i+2){
            System.out.printf("%d ",array[i]);
        }
        System.out.printf("%n");
        for(int i = 0;i < 10; i++){
            if(array[i]%2 == 0){
                System.out.printf("%d ",array[i]);
            }
        }
        System.out.printf("%n");

        for(int i = 9;i >=0; i--){
            System.out.printf("%d ",array[i]);
        }
        System.out.printf("%n");

        for(int i = 0;i < 10; i++){
            if(i == 0){
                System.out.printf("%d ",array[i]);
            }
            if(i == 9){
                System.out.printf("%d ",array[i]);
            }
        }
        System.out.printf("%n");
    }
}
