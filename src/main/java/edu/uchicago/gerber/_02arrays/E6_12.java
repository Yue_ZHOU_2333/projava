package edu.uchicago.gerber._02arrays;

import java.util.Random;
import java.util.Arrays;

public class E6_12 {
    public static void main(String[] args) {
        int[] array = new int[20];
        int max = 99;
        int min = 0;
        Random r = new Random();
        for(int i = 0;i < array.length;i++){
            array[i] = r.nextInt(max - min + 1) + min;
        }
        for(int i = 0;i < 20; i++){
            System.out.printf("%d ",array[i]);
        }
        System.out.printf("%n");
        Arrays.sort(array);
        for(int i = 0;i < 20; i++){
            System.out.printf("%d ",array[i]);
        }
        System.out.printf("%n");

    }
}
