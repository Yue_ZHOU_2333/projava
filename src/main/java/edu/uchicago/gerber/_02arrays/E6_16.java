package edu.uchicago.gerber._02arrays;

import java.util.ArrayList;
import java.util.Scanner;

public class E6_16 {
    public static void main(String[] args) {
        int n=0;
        ArrayList<Integer> values = new ArrayList<Integer>();
        Scanner in = new Scanner(System.in);
        System.out.println("Please enter the number of rows");
        int row = in.nextInt();
        System.out.println("Please enter the number");
        for(int i = 0; i < row; i++ ){
            int value = in.nextInt();
            values.add(value);
        }
        System.out.println(values);
        int[][] vertical = new int[40][row];
        for(int j = 0; j < row;j++){
            for(int i = 39; i> (39- values.get(j)); i--){
                vertical[i][j] = 1;
            }
        }
        for(int i = 0; i < 40;i++){
            for(int j = 0; j<row; j++){
                if(vertical[i][j] == 1){
                    System.out.printf("*");
                }
                else{
                    System.out.printf(" ");
                }
            }
            System.out.printf("%n");
        }
    }
}
