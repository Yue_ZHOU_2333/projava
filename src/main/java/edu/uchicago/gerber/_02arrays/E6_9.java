package edu.uchicago.gerber._02arrays;

public class E6_9 {
    public static void main(String[] args) {
        int[] a = {1,2,3,4,5};
        int[] b = {1,3,3,4,5};
        System.out.print(sameSet(a, b));
    }
    public static boolean sameSet(int[] a, int[] b){
        if (a.length != b.length){
            return false;
        }
        int length_a = a.length;
        for(int i = 0; i < length_a; i++){
            if (a[i] != b[i]){
                return false;
            }
        }
        return true;
    }
}
