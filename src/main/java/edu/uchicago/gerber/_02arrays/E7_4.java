package edu.uchicago.gerber._02arrays;

import java.awt.*;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

public class E7_4 {
    public static void main(String[] args) throws FileNotFoundException {
        Scanner console = new Scanner(System.in);
        System.out.print("Input file:");
        String inputFileName = console.next();
        System.out.print("Output file:");
        String outputFileName = console.next();

//        String inputFileName = "/Users/zhouyue/projava/src/main/java/edu/uchicago/gerber/_02arrays/input.txt";
//        String outputFileName = "/Users/zhouyue/projava/src/main/java/edu/uchicago/gerber/_02arrays/output.txt";
        File inputFile = new File(inputFileName);
        Scanner in = new Scanner(inputFile);
        PrintWriter out = new PrintWriter(outputFileName);

        double total = 0;
        int i = 1;

        while (in.hasNext()) {
            String input = "/* " + i + " */ ";
            input = input + in.nextLine();
            System.out.println(input);
            i++;
            out.print(input);
            out.printf("%n");
        }
        in.close();
        out.close();
    }
}
