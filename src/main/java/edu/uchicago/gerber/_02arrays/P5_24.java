package edu.uchicago.gerber._02arrays;

import java.util.Scanner;

public class P5_24 {
    public static void main(String[] args) {
        int total = 0;
        Scanner in = new Scanner(System.in);
        String roman = in.nextLine();
        System.out.println("The roman number is:" + roman);
        while(!roman.equals("")){
            if((roman.length() == 1) || (value(roman.charAt(0))>=value(roman.charAt(1)))){
                total = total + value(roman.charAt(0));
                roman = roman.substring(1, roman.length());
            }
            else{
                int diff = value(roman.charAt(1)) - value(roman.charAt(0));
                total = total + diff;
                roman = roman.substring(2, roman.length());
            }
        }
        System.out.printf("The value is : %d", total);
    }
    public static int value(char s){
        int value_s = 0;
        if(s == 'M'){
            value_s = 1000;
        }
        else if(s == 'D'){
            value_s = 500;
        }
        else if(s == 'C'){
            value_s = 100;
        }
        else if(s == 'L') {
            value_s = 50;
        }
        else if(s == 'X'){
            value_s = 10;
        }
        else if(s == 'V') {
            value_s = 5;
        }
        else if(s == 'I'){
            value_s = 1;
        }
        return value_s;
    }
}
