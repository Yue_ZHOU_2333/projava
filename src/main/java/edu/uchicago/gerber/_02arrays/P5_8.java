package edu.uchicago.gerber._02arrays;

import java.util.Scanner;

public class P5_8 {
    public static void main(String[] args) {
        System.out.print("Please enter the year: ");
        Scanner in = new Scanner(System.in);
        int year = in.nextInt();
        boolean result = isLeapYear(year);
        System.out.println("The leapyear is:" + result);
    }
    public static boolean isLeapYear(int year){
        if (year % 4 != 0 || (year % 100 == 0 && year % 400 != 0)) {
            return false;
        }
        return true;

    }
}
