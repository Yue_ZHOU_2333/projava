package edu.uchicago.gerber._02arrays;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

//  the main method is just for test

public class P7_5 {
    public static void main(String[] args) throws FileNotFoundException {
//        Scanner console = new Scanner(System.in);
//        System.out.print("Input file:");
//        String inputFileName = console.next();
//        System.out.print("Output file:");
//        String outputFileName = console.next();

        String inputFileName = "/Users/zhouyue/projava/src/main/java/edu/uchicago/gerber/_02arrays/input.csv";
//        String outputFileName = "/Users/zhouyue/projava/src/main/java/edu/uchicago/gerber/_02arrays/output.txt";
        File inputFile = new File(inputFileName);
        Scanner in = new Scanner(inputFile);
//        PrintWriter out = new PrintWriter(outputFileName);

        int i =numberOfRows();
        System.out.println(i);
        int j = numberOfFields(1);
        System.out.println(j);
        String fields = field(0, 0);
        System.out.println(fields);

    }
    public static int numberOfRows() throws FileNotFoundException {
        String inputFileName = "/Users/zhouyue/projava/src/main/java/edu/uchicago/gerber/_02arrays/input.csv";
        File inputFile = new File(inputFileName);
        Scanner in = new Scanner(inputFile);

        int i = 0;
        while(in.hasNext()){
            String input = in.nextLine();
//            System.out.println(input);

            i++;
        }
        in.close();
        return i;
    }
    public static int numberOfFields(int row) throws FileNotFoundException {
        String inputFileName = "/Users/zhouyue/projava/src/main/java/edu/uchicago/gerber/_02arrays/input.csv";
        File inputFile = new File(inputFileName);
        Scanner in = new Scanner(inputFile);

        int length = 0;
        int i = 0;
        while(in.hasNext()){
            String input = in.nextLine();
            String[] words = input.split(",");
//            System.out.println(input);
            if(i == row){
                length = words.length-1;
                break;
            }
            i++;
        }

        in.close();
        return length;
    }
    public static String field(int row, int column) throws FileNotFoundException {
        String inputFileName = "/Users/zhouyue/projava/src/main/java/edu/uchicago/gerber/_02arrays/input.csv";
        File inputFile = new File(inputFileName);
        Scanner in = new Scanner(inputFile);

        int length = 0;
        int i = 0;
        String target = "";
        while(in.hasNext()){
            String input = in.nextLine();
            String[] words = input.split(",");
            if(i == row){
                target = words[column];
                break;
            }
            i++;
        }

        in.close();
        return target;
    }
}
