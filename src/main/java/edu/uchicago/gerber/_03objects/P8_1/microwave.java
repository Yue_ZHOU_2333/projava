package edu.uchicago.gerber._03objects.P8_1;

public class microwave {
    private int powerLevel;
    private int seconds;

    public microwave(){
        this.powerLevel = 0;
        this.seconds = 0;
    }

    public void increaseTime(){
        this.seconds = this.seconds + 30;
    }
    public void start(){
        System.out.printf("Cooking for %d seconds at levels %d %n", this.seconds, this.powerLevel);
    }
    public void reset(){
        this.powerLevel = 0;
        this.seconds = 0;
    }
    public void chooseLevel(int level){
        this.powerLevel = level;
    }
}
