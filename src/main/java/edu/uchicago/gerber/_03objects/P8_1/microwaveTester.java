package edu.uchicago.gerber._03objects.P8_1;

public class microwaveTester {
    public static void main(String[] args) {
        microwave micro1 = new microwave();
        micro1.increaseTime();
        micro1.chooseLevel(1);
        micro1.start();
        micro1.reset();
        micro1.chooseLevel(2);
        micro1.increaseTime();
        micro1.increaseTime();
        micro1.start();
    }
}
