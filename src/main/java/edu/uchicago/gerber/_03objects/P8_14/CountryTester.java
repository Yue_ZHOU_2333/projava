package edu.uchicago.gerber._03objects.P8_14;
import java.util.*;
public class CountryTester {
    public static void main(String[] args) {
        List<Country> list=new ArrayList<Country>();
        Country us = new Country("U.S.",3787.3, 300.5);
        list.add(us);
        Country canada = new Country("Canada", 3849, 28);
        list.add(canada);
        Country china = new Country("China",3696, 1203);
        list.add(china);
        Country france = new Country("France",210, 56);
        list.add(france);
        Country italy = new Country("Italy",116.4, 58.9);
        list.add(italy);

        double max_a = 0;
        double max_p = 0;
        double max_d = 0;
        String name = "";
        for(Country res: list){
            if (res.getArea() > max_a){
                max_a = res.getArea();
                name = res.getName();
            }
        }
        System.out.printf("The country with the largest area is : %s %n", name);
        for(Country res: list){
            if (res.getPopulation() > max_p){
                max_p = res.getPopulation();
                name = res.getName();
            }
        }
        System.out.printf("The country with the largest population is : %s %n", name);
        for(Country res: list){
            if (res.getDensity() > max_p){
                max_d = res.getDensity();
                name = res.getName();
            }
        }
        System.out.printf("The country with the largest density is : %s %n", name);
    }
}
