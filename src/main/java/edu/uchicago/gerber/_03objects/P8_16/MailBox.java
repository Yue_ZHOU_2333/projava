package edu.uchicago.gerber._03objects.P8_16;

import edu.uchicago.gerber._03objects.P8_14.Country;

import java.util.ArrayList;
import java.util.List;

public class MailBox {
    private List<Message> list=new ArrayList<Message>();
    public void addMessage(Message m){
        list.add(m);
    }
    public Message getMessage(int i){
        return list.get(i);
    }
    public void removeMessage(int i){
        list.remove(i);
    }
}
