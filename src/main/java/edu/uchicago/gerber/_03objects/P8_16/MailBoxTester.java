package edu.uchicago.gerber._03objects.P8_16;

public class MailBoxTester {
    public static void main(String[] args) {
        MailBox m1 = new MailBox();
        Message text1 = new Message("Heinz","April");
        text1.append("Dear April:");
        text1.append("How are you?");
        System.out.printf(text1.toString());
        m1.addMessage(text1);
        Message text2 = new Message("April","Heinz");
        text2.append("Dear Heinz:");
        text2.append("Everything is OK.");
        m1.addMessage(text2);

        System.out.printf(m1.getMessage(1).toString());
        m1.removeMessage(0);
        System.out.printf(m1.getMessage(0).toString());

    }
}
