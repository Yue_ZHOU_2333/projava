package edu.uchicago.gerber._03objects.P8_16;



public class Message {
    private String text;
    private String sender;
    private String recipient;

    public Message(String sender, String recipient){
        this.sender = sender;
        this.recipient = recipient;
        this.text = "";
    }
    public void append(String text){
        this.text = this.text + text +"%n";
    }
    public String toString(){
        String wholeText;
        wholeText = "From:" + this.sender +"%n" + "To:" + this.recipient +"%n";
        wholeText = wholeText + this.text;
        return wholeText;
    }
}
