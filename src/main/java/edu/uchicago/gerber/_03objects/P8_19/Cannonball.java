package edu.uchicago.gerber._03objects.P8_19;

public class Cannonball {
    private double x_pos;
    private double y_pos;
    private double x_v;
    private double y_v;
    private double time = 0;
    private double y_v_ori;

    public Cannonball(double x){
        this.x_pos = x;
        this.y_pos = 0;
    }
    public void move(double sec){
        this.time = this.time + sec;
        this.x_pos = this.x_v * this.time;
        this.y_v = this.y_v_ori - 9.81 * this.time;
        this.y_pos = this.y_v_ori * this.time - 0.5 * 9.81 * this.time * this.time;
    }
    public double getX(){
        return x_pos;
    }
    public  double getY(){
        return y_pos;
    }
    public void shoot(double a, double v){
        double b = Math.toRadians(a);
        this.y_v_ori = v * Math.sin(b);
        this.x_v = v * Math.cos(b);
        this.y_v = v * Math.sin(b);

        while(this.y_pos >= 0){
            this.move(0.1);
            System.out.printf("x-position: %.2f %n", this.getX());
            System.out.printf("y-position: %.2f %n", this.getY());
        }
    }
}
