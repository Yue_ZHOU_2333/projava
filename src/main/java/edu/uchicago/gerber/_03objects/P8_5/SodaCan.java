package edu.uchicago.gerber._03objects.P8_5;

public class SodaCan {
    private int height;
    private int radius;

    public SodaCan(int h, int r){
        this.height = h;
        this.radius = r;
    }

    public double getSurfaceArea() {
        return 2 * Math.PI * this.radius * this.radius + 2 * Math.PI * this.radius * this.height;
    }

    public double getVolume() {
        return Math.PI * this.radius * this.radius * this.height;
    }
}
