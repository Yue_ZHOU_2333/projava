package edu.uchicago.gerber._03objects.P8_5;

public class SodaCanTester {
    public static void main(String[] args) {
        SodaCan can1 = new SodaCan(2, 4);
        double s1 = can1.getSurfaceArea();
        double v1 = can1.getVolume();
        System.out.printf("This can's surface area is %.2f, the volume is %.2f %n", s1, v1);
        SodaCan can2 = new SodaCan(4, 2);
        double s2 = can2.getSurfaceArea();
        double v2 = can2.getVolume();
        System.out.printf("This can's surface area is %.2f, the volume is %.2f %n", s2, v2);
    }
}
