package edu.uchicago.gerber._03objects.P8_6;

public class Car {
    private double gasLevel;
    private double eff;

    public Car(double e){
        this.eff = e;
        this.gasLevel = 0;
    }
    public void addGas(double addgas){
        this.gasLevel = this.gasLevel + addgas;
    }
    public void drive(double miles){
        this.gasLevel = this.gasLevel - miles/this.eff;
    }

    public double getGasLevel() {
        return gasLevel;
    }
}
