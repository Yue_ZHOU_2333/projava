package edu.uchicago.gerber._03objects.P8_7;

public class ComboLock {
    private static final int MAX_NUMBERS = 40;
    private int secret[] = null;
    private int State = 0;
    private int Number = 0;


    public ComboLock(int ... combination) {
        this.secret = combination;
    }

    public void reset(){
        State = 0;
    }
    public void turnLeft(int ticks) {
        Number = (Number + ticks) % MAX_NUMBERS;
        // Only compare the number when turning left the current position is odd
        if (State%2 == 1 && secret[State] == Number) {
            State = Math.min(State + 1, secret.length - 1);
        }
    }
    public void turnRight(int ticks) {
        Number = (Number + (MAX_NUMBERS - ticks % MAX_NUMBERS)) % MAX_NUMBERS;
        // Only compare the number when turning right and the current position is even
        if (State%2 == 0 && secret[State] == Number) {
            State = Math.min(State + 1, secret.length - 1);
        }
    }
    public boolean open() {
        return secret[State] == secret[secret.length - 1];
    }

    public int getCurrentNumber() {
        return Number;
    }

}

