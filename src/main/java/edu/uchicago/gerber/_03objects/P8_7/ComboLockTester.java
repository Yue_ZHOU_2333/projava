package edu.uchicago.gerber._03objects.P8_7;

public class ComboLockTester {
    public static void main(String[] args) {
        ComboLock combo = new ComboLock(22, 15, 25);
        combo.turnRight(1);
        combo.turnLeft(16);
        combo.turnRight(10);
        combo.open();
        combo = new ComboLock(39, 25, 35);
        combo.turnLeft(39);
        combo.turnRight(14);
        combo.turnLeft(40);
        combo.open();
    }
}
