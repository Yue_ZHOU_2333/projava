package edu.uchicago.gerber._03objects.P8_8;

public class VotingMachine {
    private int R_vote;
    private int D_vote;
    private int tally[] = new int[2];;

    public VotingMachine(){
        this.D_vote = 0;
        this.R_vote = 0;
    }
    public void clear(){
        this.D_vote = 0;
        this.R_vote = 0;
    }
    public void voteForR(){
        this.R_vote = this.R_vote + 1;
    }
    public void voteForD(){
        this.D_vote = this.D_vote + 1;
    }

    public int[] getTally() {
        this.tally[0] = R_vote;
        this.tally[1] = D_vote;
        return tally;
    }
}
