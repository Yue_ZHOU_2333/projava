package edu.uchicago.gerber._03objects.P8_8;

public class VotingMachineTester {
    public static void main(String[] args) {
        VotingMachine mac1 = new VotingMachine();
        mac1.voteForD();
        mac1.voteForR();
        int[] tally = mac1.getTally();
        System.out.println(tally[0]);
        System.out.println(tally[1]);
        mac1.clear();
        int[] tally2 = mac1.getTally();
        System.out.println(tally2[0]);
        System.out.println(tally2[1]);
    }
}
