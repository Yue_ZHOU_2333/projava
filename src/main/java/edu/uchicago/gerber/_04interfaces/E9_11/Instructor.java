package edu.uchicago.gerber._04interfaces.E9_11;

public class Instructor extends Person{
    private double salary;
    public Instructor(String name, int birthYear, double salary) {
        super(name, birthYear);
        this.salary = salary;
    }
    @Override
    public String toString() {
        String name_temp = getName();
        int year_temp = getBirthYear();
        return "Instructor " +
                "salary='" + salary + '\'' + ", name='" + name_temp + '\'' + ", birth year ='" + year_temp + '\'';
    }
}
