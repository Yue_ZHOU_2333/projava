package edu.uchicago.gerber._04interfaces.E9_11;

public class Person {
    private String name;
    private int birthYear;

    public Person(String name, int birthYear){
        this.birthYear = birthYear;
        this.name = name;
    }
    public String toString(){
        return name + "'s birth year is " + birthYear ;
    }
    public String getName() {
        return name;
    }
    public int getBirthYear(){
        return birthYear;
    }
}
