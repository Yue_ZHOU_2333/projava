package edu.uchicago.gerber._04interfaces.E9_11;

public class Student extends Person{
    private String major;
    public Student(String name, int birthYear, String major) {
        super(name, birthYear);
        this.major = major;
    }


    @Override
    public String toString() {
        String name_temp = getName();
        int year_temp = getBirthYear();
        return "Student " +
                "major='" + major + '\'' + ", name='" + name_temp + '\'' + ", birth year ='" + year_temp + '\'';
    }
}
