package edu.uchicago.gerber._04interfaces.E9_11;

public class Tester {
    public static void main(String[] args) {
        Student stu1 = new Student("zhou", 1999, "computer science");
        System.out.println(stu1.toString());
        Person p1 = new Person("yue",1980);
        System.out.println(p1.toString());
        Instructor in1 = new Instructor("zyy",1998, 24000);
        System.out.println(in1.toString());
    }
}
