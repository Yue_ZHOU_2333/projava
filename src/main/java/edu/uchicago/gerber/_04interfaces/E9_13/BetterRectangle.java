package edu.uchicago.gerber._04interfaces.E9_13;

import java.awt.*;

public class BetterRectangle extends Rectangle {
    public BetterRectangle(int x, int y, int height, int width){
        super.setLocation(x, y);
        super.setSize(width, height);
    }
    public double getPerimeter(){
        return super.getHeight() * 2 + super.getWidth() * 2;
    }
    public double getArea(){
        return super.getHeight() * super.getWidth();
    }
}
