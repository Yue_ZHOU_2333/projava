package edu.uchicago.gerber._04interfaces.E9_13;

public class Tester {
    public static void main(String[] args) {
        BetterRectangle r1 = new BetterRectangle(1, 2,3,4);
        System.out.println(r1.getArea());
        System.out.println(r1.getPerimeter());
    }
}
