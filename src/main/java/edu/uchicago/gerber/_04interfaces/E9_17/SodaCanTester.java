package edu.uchicago.gerber._04interfaces.E9_17;

public class SodaCanTester {
    public static void main(String[] args) {
        Measurable[] SodaCans = new Measurable[3];
        SodaCans[0] = new SodaCan(1,1);
        SodaCans[1] = new SodaCan(1,2);
        SodaCans[2] = new SodaCan(1,3);
        System.out.println("Average of surface area:" + average(SodaCans));
    }
    public static double average(Measurable[] objects){
        if (objects.length == 0){
            return 0;
        }
        double sum = 0;
        for (Measurable obj: objects){
            sum = sum + obj.getMeasure();
        }
        return sum / objects.length;
    }
}


