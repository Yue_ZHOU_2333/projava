package edu.uchicago.gerber._04interfaces.E9_8;

public class BasicAccount extends BankAccount{
    private double balance = getBalance();
    public void withdraw(double amount){
        balance = getBalance();
        if (balance <= amount){
            System.out.printf("The account balance is insufficient, only $ %.2f can be withdrawn. %n", balance);
        }
        else{
            super.withdraw(amount);
            System.out.printf("$ %.2f has been withdrawn %n", amount);
        }
        balance = getBalance();
    }
}
