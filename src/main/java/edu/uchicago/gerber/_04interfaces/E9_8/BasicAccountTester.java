package edu.uchicago.gerber._04interfaces.E9_8;

public class BasicAccountTester {
    public static void main(String[] args) {
        BasicAccount a = new BasicAccount();
        a.deposit(100);
        a.withdraw(50);
        System.out.println(a.getBalance());
        a.withdraw(100);
        System.out.println(a.getBalance());
    }
}
