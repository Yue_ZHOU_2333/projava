package edu.uchicago.gerber._04interfaces.P9_1;

public class Tester {
    public static void main(String[] args) {
        Clock a = new Clock();
        WorldClock b = new WorldClock(3);
        System.out.println(a.getTime());
        System.out.println(b.getTime());
    }
}
