package edu.uchicago.gerber._04interfaces.P9_1;

public class WorldClock extends Clock{
    private int zone;
    public WorldClock(int zone){
        this.zone = zone;
    }

    @Override
    public String getHour() {
        int time = Integer.parseInt(super.getHour()) + this.zone;
        return String.valueOf(time);
    }

}
