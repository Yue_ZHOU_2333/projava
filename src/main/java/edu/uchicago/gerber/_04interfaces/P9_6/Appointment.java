package edu.uchicago.gerber._04interfaces.P9_6;

public class Appointment {

    public int dayNum;
    public int monthNum;
    public int yearNum;
    public String desc;

    public Appointment(String des, int day){
        desc = des;
        dayNum = day;
    }
    public Appointment(String des, int day, int month){
        desc = des;
        dayNum = day;
        monthNum = month;
    }
    public Appointment(String des, int day, int month, int year) {
        desc = des;
        dayNum = day;
        monthNum = month;
        yearNum = year;
    }
    boolean occursOn(int day, int month, int year){
        if( (this.dayNum == day) && (this.monthNum == month) && (this.yearNum == year)) {
            return true;
        }
        return false;
    }
}
