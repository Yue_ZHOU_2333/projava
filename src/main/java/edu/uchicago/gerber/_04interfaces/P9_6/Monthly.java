package edu.uchicago.gerber._04interfaces.P9_6;

public class Monthly extends Appointment{
    public Monthly(String des, int monum){
        super(des, monum);
    }
    public String getDesc(){
        return desc;
    }
    boolean occursOn(int day, int month, int year){
        if( (this.dayNum == day) )
            return true;
        return false;

    }
}