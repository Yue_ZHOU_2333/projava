package edu.uchicago.gerber._04interfaces.P9_6;

public class OneTime extends Appointment{
    public OneTime(String des, int day, int month, int year) {
        super(des, day, month, year);
    }
    public String getDesc(){
        return desc;
    }
}
