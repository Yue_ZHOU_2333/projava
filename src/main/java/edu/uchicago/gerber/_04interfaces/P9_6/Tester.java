package edu.uchicago.gerber._04interfaces.P9_6;

import java.util.ArrayList;
import java.util.List;

public class Tester {
    public static void main(String[] args) {
        List<Appointment> aps = new ArrayList<>(); //add something into it
        Monthly a = new Monthly("Have Lunch", 2);
        aps.add(a);
        Daily b = new Daily("see dentist", 4,3);
        aps.add(b);
        for (Appointment ap : aps){
            if(ap.occursOn(7, 3, 2021)){
                System.out.println("Have the appointment!" + ap.desc);
            }
            else{
                System.out.println("No appointment!");
            }
        }
    }

}
