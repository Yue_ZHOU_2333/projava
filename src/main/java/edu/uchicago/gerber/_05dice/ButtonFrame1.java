package edu.uchicago.gerber._05dice;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ButtonFrame1 extends JFrame {
    private JButton button;
    private JLabel label;
    private static final int FRAME_WIDTH = 100;
    private static final int FRAME_HEIGHT = 60;
    public ButtonFrame1() {
        createComponents();
        setSize(FRAME_WIDTH, FRAME_HEIGHT);
}
    private void createComponents() {
        JButton button = new JButton("Print Blue");
        JPanel panel = new JPanel();
        panel.add(button);
        add(panel);

        ActionListener listener = new ClickListener();
        button.addActionListener(listener);
    }
    class ClickListener implements ActionListener {
        public void actionPerformed(ActionEvent event){
            System.out.println("I was clicked.");
        }
    }
}
