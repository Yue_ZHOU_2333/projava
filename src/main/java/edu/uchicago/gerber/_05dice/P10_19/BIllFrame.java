package edu.uchicago.gerber._05dice.P10_19;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

public class BIllFrame extends JFrame {
    private JTextField itemField;
    private JTextField valueField;
    private JButton addButton;
    private JButton item1Button;
    private JButton item2Button;
    private JButton item3Button;
    private JButton item4Button;
    private JButton item5Button;
    private JButton item6Button;
    private JButton item7Button;
    private JButton item8Button;
    private JButton item9Button;
    private JButton item10Button;
    private JButton generateBill;
    private JTextArea resultArea;
    private double sum = 0;
    private double tips;
    private double tax;
    private List<Double> lists = new ArrayList<Double>();

    class AddBillListener implements ActionListener{
        public void actionPerformed(ActionEvent event){
            String label = itemField.getText();
            double value = Double.parseDouble(valueField.getText());
            sum = sum + value;
            resultArea.append("The sum is:" + sum + "\n");
        }
    }
    class AddItem1Listener implements ActionListener{
        public void actionPerformed(ActionEvent event){
            double value = 13.95;
            sum = sum + value;
            resultArea.append("The sum is:" + sum + "\n");
        }
    }
    class AddItem2Listener implements ActionListener{
        public void actionPerformed(ActionEvent event){
            double value = 6.95;
            sum = sum + value;
            resultArea.append("The sum is:" + sum + "\n");
        }
    }
    class AddItem3Listener implements ActionListener{
        public void actionPerformed(ActionEvent event){
            double value = 7.25;
            sum = sum + value;
            resultArea.append("The sum is:" + sum + "\n");
        }
    }
    class AddItem4Listener implements ActionListener{
        public void actionPerformed(ActionEvent event){
            double value = 9.45;
            sum = sum + value;
            resultArea.append("The sum is:" + sum + "\n");
        }
    }
    class AddItem5Listener implements ActionListener{
        public void actionPerformed(ActionEvent event){
            double value = 7.95;
            sum = sum + value;
            resultArea.append("The sum is:" + sum + "\n");
        }
    }
    class AddItem6Listener implements ActionListener{
        public void actionPerformed(ActionEvent event){
            double value = 7.95;
            sum = sum + value;
            resultArea.append("The sum is:" + sum + "\n");
        }
    }
    class AddItem7Listener implements ActionListener{
        public void actionPerformed(ActionEvent event){
            double value = 1.8;
            sum = sum + value;
            resultArea.append("The sum is:" + sum + "\n");
        }
    }
    class AddItem8Listener implements ActionListener{
        public void actionPerformed(ActionEvent event){
            double value = 3.2;
            sum = sum + value;
            resultArea.append("The sum is:" + sum + "\n");
        }
    }
    class AddItem9Listener implements ActionListener{
        public void actionPerformed(ActionEvent event){
            double value = 3.5;
            sum = sum + value;
            resultArea.append("The sum is:" + sum + "\n");
        }
    }
    class AddItem10Listener implements ActionListener{
        public void actionPerformed(ActionEvent event){
            double value = 1.9;
            sum = sum + value;
            resultArea.append("The sum is:" + sum + "\n");
        }
    }
    class generateBillListener implements ActionListener{
        public void actionPerformed(ActionEvent event){
            resultArea.setText("The total prices is:"+ sum + "\n" + "The tax is:" + sum * 0.1 + "\n" + "The tips is:" +
                    sum * 0.15 + "\n");
            resultArea.setEditable(false);

        }
    }
    private void createButton(){
        addButton = new JButton("add");
        addButton.addActionListener(new AddBillListener());
        item1Button = new JButton("N.Y.Steak & Egg, $13.95");
        item1Button.addActionListener(new AddItem1Listener());
        item2Button = new JButton("Two Eggs,Bacon, or Sausage, $6.95");
        item2Button.addActionListener(new AddItem2Listener());
        item3Button = new JButton("Two Pancake, $7.25");
        item3Button.addActionListener(new AddItem3Listener());
        item4Button = new JButton("Steak Omelette, $9.45");
        item4Button.addActionListener(new AddItem4Listener());
        item5Button = new JButton("Mediterranean Omelette, $7.95");
        item5Button.addActionListener(new AddItem5Listener());
        item6Button = new JButton("All Vegi-Egg White Omelette, $7.95");
        item6Button.addActionListener(new AddItem6Listener());
        item7Button = new JButton("Coffee, $1.8");
        item7Button.addActionListener(new AddItem7Listener());
        item8Button = new JButton("Latte, $3.2");
        item8Button.addActionListener(new AddItem8Listener());
        item9Button = new JButton("White Mocha, $3.5");
        item9Button.addActionListener(new AddItem9Listener());
        item10Button = new JButton("Hot Tea, $1.9");
        item10Button.addActionListener(new AddItem10Listener());
        generateBill = new JButton("generate bill");
        generateBill.addActionListener(new generateBillListener());
    }
    private static final int FRAME_WIDTH = 300;
    private static final int FRAME_HEIGHT = 800;

    private static final int AREA_ROWS = 15;
    private static final int AREA_COLUMNS = 25;
    private static final String DEFAULT_LABEL = "Enter dish";
    private static final double DEFAULT_VALUE = 100;

    public BIllFrame(){
        resultArea = new JTextArea(AREA_ROWS, AREA_COLUMNS);
        resultArea.setText(" ");
        resultArea.setEditable(false);
        createTextField();
        createButton();
        createPanel();
        setSize(FRAME_WIDTH, FRAME_HEIGHT);
    }
    private void createTextField() {
        final int FIELD_WIDTH = 20;
        itemField = new JTextField(FIELD_WIDTH);
        itemField.setText(DEFAULT_LABEL);
        valueField = new JTextField(FIELD_WIDTH);
        valueField.setText("Enter the price");
    }
    private void createPanel() {
        JPanel panel = new JPanel();
        panel.add(itemField);
        panel.add(valueField);
        panel.add(addButton);
        panel.add(item1Button);
        panel.add(item2Button);
        panel.add(item3Button);
        panel.add(item4Button);
        panel.add(item5Button);
        panel.add(item6Button);
        panel.add(item7Button);
        panel.add(item8Button);
        panel.add(item9Button);
        panel.add(item10Button);
        panel.add(generateBill);
        JScrollPane scrollPane = new JScrollPane(resultArea);
        panel.add(scrollPane);
        add(panel);
    }
}
