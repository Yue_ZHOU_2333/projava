package edu.uchicago.gerber._05dice.P10_9;

import javax.swing.*;
import java.awt.*;

public class FlagComponent extends JComponent
{
    // Use instance variables for components
    public void paintComponent(Graphics g){
        g.setColor(Color.GREEN);
        g.fillRect(50,100,30,60);

        g.setColor(Color.WHITE);
        g.fillRect(80,100,30,60);

        g.setColor(Color.RED);
        g.fillRect(110,100,30,60);

        g.setColor(Color.BLACK);
        g.fillRect(150,100,90,20);

        g.setColor(Color.RED);
        g.fillRect(150,120,90,20);

        g.setColor(Color.ORANGE);
        g.fillRect(150,140,90,20);

        g.setColor(Color.RED);
        g.fillRect(250,100,90,20);

        g.setColor(Color.WHITE);
        g.fillRect(250,120,90,20);

        g.setColor(Color.GREEN);
        g.fillRect(250,140,90,20);
    }
}
