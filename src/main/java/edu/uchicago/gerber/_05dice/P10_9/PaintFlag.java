package edu.uchicago.gerber._05dice.P10_9;

import javax.swing.*;

public class PaintFlag {
    public static void main(String[] args) {
        JFrame frame = new JFrame();

        frame.setSize(400, 200);
        frame.setTitle("Italian Flag, German Flag, Hungarian Flag");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        JComponent component = new FlagComponent();
        frame.add(component);

        frame.setVisible(true);
    }
}
