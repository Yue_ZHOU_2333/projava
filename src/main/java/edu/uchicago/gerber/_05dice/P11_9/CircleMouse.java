package edu.uchicago.gerber._05dice.P11_9;

import edu.uchicago.gerber._05dice.P10_9.FlagComponent;

import javax.swing.*;
import java.io.IOException;

public class CircleMouse {
    public static void main(String[] args) throws IOException {
        JFrame frame = new JFrame("Draw Circle");
        JComponent component = new PointComponent();
        frame.add(component);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
