package edu.uchicago.gerber._05dice.P11_9;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;

public class PointComponent extends JComponent {
    Point p; int r;
    {
        addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent e) {
                p = e.getPoint(); r = 0; repaint();
            }
            public void mouseReleased(MouseEvent e) {
                r = (int) Math.round(e.getPoint().distance(p));
                repaint();
            }
        });
        addMouseMotionListener(new MouseMotionAdapter() {
                public void mouseDragged(MouseEvent e) {
                    r = (int) Math.round(e.getPoint().distance(p));
                    repaint();
                }
            });
        setPreferredSize(new Dimension(400, 300));
    }
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        if(p != null) {
            g.setColor(Color.GREEN);
            g.fillOval(p.x - r, p.y - r, 2 * r, 2 * r);
        }
    }
}



