package edu.uchicago.gerber._05dice.P11_9;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;

public class pointComponentWith2Click extends JComponent{
    Point p1, p2;
    {
        addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                if (p1 == null || p2 != null) {
                    p1 = e.getPoint();
                    p2 = null;
                } else {
                    p2 = e.getPoint();
                }
                repaint();
            }
        });
        setPreferredSize(new Dimension(400, 300));
    }
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        if(p1 != null && p2 != null) {
            int r = (int) Math.round(p1.distance(p2));
            g.drawOval(p1.x - r, p1.y - r, 2 * r, 2 * r);
        }
    }
}
