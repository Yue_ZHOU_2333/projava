package edu.uchicago.gerber._05dice.pig;

import java.util.Random;

import java.util.Random;

public class Dice {

    private int dFaceValue;

    public int throwDice() {
        Random ran = new Random();
        int nRand = ran.nextInt(6) + 1;
        dFaceValue = nRand;
        return nRand;
    }

}
