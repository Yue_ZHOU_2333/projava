package edu.uchicago.gerber._05dice.pig;
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;

public class GUI extends JFrame{
    private JTextField nameField;
    private JButton rollButton;
    private JButton holdButton;
    private JButton startButton;
    private JTextArea resultArea;
    private JScrollPane scrollPane;

    private player human;
    private computer comp;
    private Dice die;


    public GUI() {

        human = new player();
        comp = new computer();
        die = new Dice();

        resultArea = new JTextArea(15, 20);
        resultArea.setText(" ");
        resultArea.setEditable(false);
        createTextField();
        createUIComponents();
        createPanel();
        setSize(1000, 1000);

    }

    public void playerGame() {
        int nRoll = die.throwDice();
        if (nRoll == 1) {
            human.setScore(0);
            resultArea.append("\n>> You rolled a 1. Your score is now 0.\nNow it is the Computer's turn.");
            computerGame();
        } else {
            human.setScore(nRoll);
            resultArea.append("\n>> You rolled a " + nRoll + ".\nYour score is now " + human.getScore() + ".");
            resultArea.append("\n>> Press 'Roll' to continue or 'Hold' to end your turn.");
        }
        if (human.getScore() >= 100 || comp.getScore() >= 100) {
            announceWinner();
        }
    }

    public void computerGame() {

        Random ran = new Random();
        ArrayList<Integer> rolls = new ArrayList<Integer>();
        for (int nC = 0; nC < ran.nextInt(10) + 1; nC++) {
            rolls.add(ran.nextInt(6) + 1);
        }

        for (int i = 0; i < rolls.size(); i++) {
            if (rolls.get(i) == 1) {
                comp.setScore(0);
                resultArea.append("\nComputer rolled a 1. Computer's score is 0.\nNow it is your turn.");
                playerGame();
                return;
            } else {
                comp.setScore(rolls.get(i));
                resultArea.append("\nComputer rolled a " + rolls.get(i) + ".\nComputer's score is " + comp.getScore() + ".");
            }
        }
        if (human.getScore() >= 100 || comp.getScore() >= 100) {
            announceWinner();
        }
        resultArea.append("\nComputer's turn is finished. Now it is your turn.");
        playerGame();
    }

    public void announceWinner() {
        if (comp.getScore() > human.getScore()) {
            resultArea.append("\nComputer Wins :-(");
        } else {
            resultArea.append("\nYou Win!");
        }
    }
    private void createTextField() {
        final int FIELD_WIDTH = 20;
        nameField = new JTextField(FIELD_WIDTH);
        nameField.setText("Enter your name");
    }
    private void createUIComponents() {
        startButton = new JButton("start!");
        startButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String playerName = nameField.getText();
                human.setName(playerName);
                resultArea.setText("Welcome to Pig Dice, " + playerName + "!");
                playerGame();
            }
        });

        rollButton = new JButton("roll!");
        rollButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (comp.getScore() < 100 && human.getScore() < 100) {
                    playerGame();
                } else {
                    announceWinner();
                }
            }
        });

        holdButton = new JButton("hold!");
        holdButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (comp.getScore() < 100 && human.getScore() < 100) {
                    computerGame();
                } else {
                    announceWinner();
                }
            }
        });
    }
    private void createPanel() {
        JPanel panel = new JPanel();
        panel.add(nameField);
        panel.add(rollButton);
        panel.add(holdButton);
        panel.add(startButton);
        panel.add(resultArea);
        JScrollPane scrollPane = new JScrollPane(resultArea);
        panel.add(scrollPane);
        add(panel);
    }

}

