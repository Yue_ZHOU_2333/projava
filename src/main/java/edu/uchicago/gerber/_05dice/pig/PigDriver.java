package edu.uchicago.gerber._05dice.pig;

import javax.swing.*;

public class PigDriver {
    static final int GAMES_PLAYED = 10000000;
    public static void main(String[] args) {
        JFrame frame = new GUI();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(600, 600);
        frame.setVisible(true);
    }
}

