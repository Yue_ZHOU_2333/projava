package edu.uchicago.gerber._05dice.pig;

public class computer implements play {

    private int score;
    private String name;

    public computer() {
        this.name = "Computer";
        this.score = 0;
    }

    public int getScore() {
        return this.score;
    }

    public void setScore(int s) {
        if (s==0) {
            this.score = 0;
        }
        this.score += s;
    }

}
