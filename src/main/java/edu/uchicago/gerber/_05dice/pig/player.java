package edu.uchicago.gerber._05dice.pig;

public class player implements play {

    private String name;
    private int score;

    public player() {
        this.name = "";
        this.score = 0;
    }

    public void setName(String n) {
        this.name = n;
    }

    public String getName() {
        return this.name;
    }

    public int getScore() {
        return this.score;
    }

    public void setScore(int s) {
        if (s==0) {
            this.score = 0;
        }
        this.score += s;
    }

}