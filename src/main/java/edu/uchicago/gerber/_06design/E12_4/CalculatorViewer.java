package edu.uchicago.gerber._06design.E12_4;

import javax.swing.*;

public class CalculatorViewer {
    public static void main(String[] args) {
        JFrame frame = new CalculatorFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setTitle("Calculator");
        frame.setVisible(true);
    }
}
