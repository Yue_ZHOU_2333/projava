package edu.uchicago.gerber._06design.E12_4;

public class LevelThree {
    private int x;
    private int y;
    private int answer;

    public LevelThree(){
        this.x = 0;
        this.y = 0;
        this.answer = 0;
    }
    public void SetQuestion(){
        this.x = (int)(Math.random()*10);
        this.y = (int)(Math.random()*10);
        if(y>x){
            int tmp = this.y;
            this.y = this.x;
            this.x = tmp;
        }
        this.answer = x - y;
    }
    public Boolean CheckAnswer(int a){
        if(a==this.answer){
            return true;
        }
        else{
            return false;
        }
    }
    public void display(){
        System.out.printf("%d - %d = ?%n", x, y);
    }
}
