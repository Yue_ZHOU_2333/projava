package edu.uchicago.gerber._06design.E12_4;

import java.util.Scanner;

public class driver {
    public static void main(String[] args) {
        int score = 0;
        LevelOne l1 = new LevelOne();
        LevelTwo l2 = new LevelTwo();
        LevelThree l3 = new LevelThree();

        while(score < 5){
            l1.setQuestion();
            l1.display();
            Scanner in = new Scanner(System.in);
            int answer = in.nextInt();
            if(l1.CheckAnswer(answer)){
                score++;
            }
        }
        score = 0;
        while(score < 5){
            l2.setQuestion();
            l2.display();
            Scanner in = new Scanner(System.in);
            int answer = in.nextInt();
            if(l2.CheckAnswer(answer)){
                score++;
            }
        }
        score = 0;
        while(score < 5){
            l3.SetQuestion();
            l3.display();
            Scanner in = new Scanner(System.in);
            int answer = in.nextInt();
            if(l3.CheckAnswer(answer)){
                score++;
            }
        }
    }
}
