package edu.uchicago.gerber._06design.P12_1;

public class Coin {
    private double money;
    public Coin(double value) {
        money = value;
    }

    public Coin(String  startElement){
        startElement = startElement.toUpperCase();
        double dVal = 0.0;

        switch (startElement){
            case "Q": dVal = 0.25;
                break;
            case "D": dVal = 0.10;
                break;
            case "N": dVal = 0.05;
                break;
            default: dVal = 0.00;
                break;
        }
        setValue(dVal);
    }
    public double getValue() {
        return money;
    }
    public void setValue(double value) {
        money = value;
    }
    public String toString(){
        return VendMachine.string_DecimalFormat.format(getValue());
    }
}
