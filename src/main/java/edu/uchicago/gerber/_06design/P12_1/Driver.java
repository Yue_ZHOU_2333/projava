package edu.uchicago.gerber._06design.P12_1;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Driver {
    private static Scanner in = new Scanner(System.in);
    public static void main(String[] args) {
        VendMachine vndMachine = new VendMachine();
        List<Product> prdProducts = new ArrayList<>();
        prdProducts.add(new Product("N.Y.Steak & Egg", 13.95));
        prdProducts.add(new Product("Two Eggs,Bacon, or Sausage", 6.95));
        prdProducts.add(new Product("Two Pancake", 7.25));
        prdProducts.add(new Product("Steak Omelette", 9.45));
        prdProducts.add(new Product("Mediterranean Omelette", 7.95));
        prdProducts.add(new Product("All Vegi-Egg White Omelette", 7.95));
        prdProducts.add(new Product("Coffee", 1.8));
        prdProducts.add(new Product("Latte", 3.2));
        prdProducts.add(new Product("White Mocha", 3.5));
        vndMachine.stockMe(prdProducts);

        outer:
        while (true) {
            System.out.println("Type 'i' for insert coins or 's' for select product or type 'exit' to quit");
            String initial = in.nextLine();
            initial = initial.toUpperCase();
            String strMoney, strSelect;
            switch (initial){
                case "I":
                    strMoney = addMoney();
                    vndMachine.insertCoins(strMoney);
                    System.out.println("Credits : " + vndMachine.howMuchInserted());
                    break;
                case "S":
                    System.out.println(vndMachine.showSelection());
                    strSelect = makeSelection();
                    if (strSelect.equalsIgnoreCase("AABBCC")){
                        List<Coin> conCashOuts = vndMachine.cashOut();
                        System.out.println("Jackpot!");
                        for (Coin conCashOut : conCashOuts) {
                            System.out.println(conCashOut);
                        }
                        break;
                    }
                    Product prdProduct = vndMachine.vend(strSelect);
                    if (prdProduct != null)
                        System.out.println("Thank you and Enjoy: " + prdProduct);
                    else {
                        System.out.print("You inserted " + vndMachine.howMuchInserted() + " : Insufficient coins or out of stock");
                        List<Coin> conReturns = vndMachine.returnCoins();
                        System.out.println(", here is your money back: ");
                        for (Coin conReturn : conReturns) {
                            System.out.println(conReturn);
                        }

                    }
                    break;
                case "EXIT":
                    break outer;
                default:
                    break;
            }

        }
        System.out.println("Thank you for vending");

    }

    private static String addMoney(){

        System.out.println("Insert Coins : Q D N");
        return in.nextLine().toUpperCase();
    }

    private static String makeSelection(){
        System.out.println("Please choose a product such as 'A2'");
        return in.nextLine().toUpperCase();

    }
}
