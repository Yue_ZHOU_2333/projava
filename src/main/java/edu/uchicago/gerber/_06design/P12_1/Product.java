package edu.uchicago.gerber._06design.P12_1;

public class Product {
    private String Desc;
    private double Price;


    public Product(String desc, double price) {
        this.Desc = desc;
        this.Price = price;
    }

    public String getDesc() {
        return Desc;
    }

    public void setDesc(String desc) {
        this.Desc = desc;
    }

    public double getPrice() {
        return Price;
    }

    public void setPrice(double price) {
        this.Price = price;
    }

    public String toString() {
        return  Desc + " " + VendMachine.string_DecimalFormat.format(Price);
    }
}
