package edu.uchicago.gerber._06design.P12_1;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class VendMachine {

    public static final DecimalFormat string_DecimalFormat = new DecimalFormat("$0.00");
    private Map<String, Product[]> map_Products;
    private List<Coin> total;
    private List<Coin> Purchase_Moneys;
    public static final int ROWS = 3;
    public static final int COLS = 3;
    public static final int SLOTS = 3;

    public VendMachine() {
        total = new ArrayList<>();
        Purchase_Moneys = new ArrayList<>();
    }

    public void stockMe(List<Product> Products){
        map_Products = new TreeMap<>();
        int Counter = 0;
        Product[] stocks;
        Product item;
        outerloop:
        for (int r = 0; r < ROWS ; r++) {
            for (int c = 0; c < COLS ; c++) {
                stocks = new Product[SLOTS];

                if (Counter < Products.size()){
                    item = Products.get(Counter++);
                } else {
                    break outerloop;
                }
                for (int s = 0; s < SLOTS ; s++) {
                    stocks[s] = item;
                }
                map_Products.put(getProperId(r, c), stocks);

            }
        }
    }
    public String showSelection(){
        StringBuilder stb = new StringBuilder();
        Product[] Products;
        for (String strKey : map_Products.keySet()) {
            Products =  map_Products.get(strKey);
            stb.append(strKey + " : ");
            boolean bOutStock = true; //assume it's out of stock
            for (Product prdProduct : Products) {
                if (prdProduct != null){
                    bOutStock = false;
                    stb.append(prdProduct.toString());
                    stb.append("\n");
                    break;
                }
            }
            if (bOutStock){
                stb.append("Out of Stock");
                stb.append("\n");
            }
        }
        stb.append("Credits : " + howMuchInserted() + "\n");
        return stb.toString();
    }

    public void insertCoins(List<Coin> conPassed){
        for (Coin con : conPassed) {
            Purchase_Moneys.add(con);
        }
    }

    public void insertCoins(String strCoinValue){
        List<Coin> conCoins = new ArrayList<>();
        String[] strCoins = strCoinValue.split(" ");
        for (String strCoin : strCoins) {
            Purchase_Moneys.add(new Coin(strCoin));
        }
        insertCoins(conCoins);
    }

    private double getFunds(){
        double dReturn = 0.0;
        for (Coin conMoney : Purchase_Moneys) {
            dReturn += conMoney.getValue();
        }
        return dReturn;
    }
    public String howMuchInserted(){
        double dReturn = 0.0;
        for (Coin purchaseMoney : Purchase_Moneys) {
            dReturn += purchaseMoney.getValue();
        }
        return string_DecimalFormat.format(dReturn);
    }

    public Product vend(String strKey){

        Product prdReturn = null;
        Product[] prdProds = map_Products.get(strKey);
        if (prdProds == null){
            return prdReturn;
        }

        for (int c = 0; c <prdProds.length ; c++) {
            if(prdProds[c] != null){
                if (getFunds() >= prdProds[c].getPrice()){
                    transferFunds();
                    prdReturn =   prdProds[c];
                    prdProds[c] = null;
                }
                break;
            }
        }
        return prdReturn;
    }


    public List<Coin> returnCoins(){
        List<Coin> conTemps = Purchase_Moneys;
        Purchase_Moneys = new ArrayList<>();
        return conTemps;
    }

    public List<Coin> cashOut(){
        List<Coin> conTemp = total;
        total = new ArrayList<>();
        return conTemp;
    }

    private void transferFunds(){
        for (Coin conMoney : Purchase_Moneys) {
            total.add(conMoney);
        }
        Purchase_Moneys = new ArrayList<>();
    }

    private String getProperId(int nRow, int nCol){
        nCol++;
        switch (nRow){
            case 0:
                return "A" + nCol;
            case 1:
                return "B" + nCol;
            case 2:
                return "C" + nCol;
            default:
                return "";
        }
    }
}
