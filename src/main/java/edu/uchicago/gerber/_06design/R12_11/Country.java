package edu.uchicago.gerber._06design.R12_11;

public class Country {
    private String name;
    private double area;
    private double population;
    private double density;

    private static int numCountries = 0;
    public Country (String name, double a, double p) {
        this.name = name;
        this.area = a;
        this.population = p;
        this.numCountries = this.numCountries + 1 ;
    }
    public String getName() {
        return this.name;
    }
    public double getArea() {
        return area;
    }
    public double getPopulation() {
        return population;
    }
    public double getDensity() {
        return population/area;
    }
    public static int getNumCountries() {
        return numCountries;
    }
}