package edu.uchicago.gerber._06design.R12_11;

import edu.uchicago.gerber._03objects.P8_14.Country;

import java.util.ArrayList;
import java.util.List;

public class CountryTester {
    public static void main(String[] args) {
        List<edu.uchicago.gerber._03objects.P8_14.Country> list=new ArrayList<edu.uchicago.gerber._03objects.P8_14.Country>();
        edu.uchicago.gerber._03objects.P8_14.Country us = new edu.uchicago.gerber._03objects.P8_14.Country("U.S.",3787.3, 300.5);
        list.add(us);
        edu.uchicago.gerber._03objects.P8_14.Country canada = new edu.uchicago.gerber._03objects.P8_14.Country("Canada", 3849, 28);
        list.add(canada);
        edu.uchicago.gerber._03objects.P8_14.Country china = new edu.uchicago.gerber._03objects.P8_14.Country("China",3696, 1203);
        list.add(china);
        edu.uchicago.gerber._03objects.P8_14.Country france = new edu.uchicago.gerber._03objects.P8_14.Country("France",210, 56);
        list.add(france);
        edu.uchicago.gerber._03objects.P8_14.Country italy = new edu.uchicago.gerber._03objects.P8_14.Country("Italy",116.4, 58.9);
        list.add(italy);

        double max_a = 0;
        double max_p = 0;
        double max_d = 0;
        String name = "";
        for(edu.uchicago.gerber._03objects.P8_14.Country res: list){
            if (res.getArea() > max_a){
                max_a = res.getArea();
                name = res.getName();
            }
        }
        System.out.printf("The country with the largest area is : %s %n", name);
        for(edu.uchicago.gerber._03objects.P8_14.Country res: list){
            if (res.getPopulation() > max_p){
                max_p = res.getPopulation();
                name = res.getName();
            }
        }
        System.out.printf("The country with the largest population is : %s %n", name);
        for(Country res: list){
            if (res.getDensity() > max_p){
                max_d = res.getDensity();
                name = res.getName();
            }
        }
        System.out.printf("The country with the largest density is : %s %n", name);
    }
}
