package edu.uchicago.gerber._06design.R12_13;

public class Product {
    private String mDesc;
    private double mPrice;


    public Product(String desc, double price) {
        mDesc = desc;
        mPrice = price;
    }

    public String getDesc() {
        return mDesc;
    }

    public void setDesc(String desc) {
        mDesc = desc;
    }

    public double getPrice() {
        return mPrice;
    }

    public void setPrice(double price) {
        mPrice = price;
    }

    @Override
    public String toString() {
        return  mDesc + " " + VendMachine.sDecimalFormat.format(mPrice);
    }
}
