package edu.uchicago.gerber._07streams.E13_20;

import java.util.ArrayList;

public class bills {
    public static void main(String[] args) {
        makeChange(100);
    }
    static void makeChange(int n) {
        ArrayList<Integer> coins = new ArrayList<Integer>();  // you should declare as List, not ArrayList
        coins.add(0);
        coins.add(0);
        coins.add(0);
        coins.add(0);
        System.out.println(" $1  $5  $20  $100");
        System.out.println("------------");
        makeChange(n, 0, coins);
    }

    static void makeChange(int n, int i, ArrayList<Integer> coins) {
        int sum = getSum(coins);
        if (sum == n) {
            System.out.println(coins.toString());
        } else {
            while (i < coins.size()) {
                coins.set(i, coins.get(i) + 1);
                if (getSum(coins) <= n) {
                    makeChange(n, i, coins);
                }
                coins.set(i, coins.get(i) - 1);
                ++i;
            }
        }
    }
    static int getSum(ArrayList<Integer> coins) {
        int sum = coins.get(0);
        sum += 5 * coins.get(1);
        sum += 20 * coins.get(2);
        sum += 100 * coins.get(3);
        return sum;
    }
}
