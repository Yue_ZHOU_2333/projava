package edu.uchicago.gerber._07streams.E13_4;

public class binary {
    public static void main(String[] args) {
        System.out.println(binaryConvert(17));
        System.out.println(binaryConvert("10001"));
    }
    static String binaryConvert(int num){
        if(num == 0) {
            return "0";
        }
        if (num == 1){
            return "1";
        }
        int mod = num % 2;
        int res = num / 2;
        return binaryConvert(res) + mod;
    }
    static int binaryConvert(String num){
        return binaryConvert(num, 1);
    }
    static int binaryConvert(String num, int n){
        int len = num.length();
        if(len == 1){
            if(Integer. parseInt(num) == 1){
                return 1 * n;
            }
            else{
                return 0;
            }
        }
        String last = num.substring(len-1);
        int res = n * Integer. parseInt(last);
        n = n * 2;
        return binaryConvert(num.substring(0,len-1), n) + res;
    }
}
