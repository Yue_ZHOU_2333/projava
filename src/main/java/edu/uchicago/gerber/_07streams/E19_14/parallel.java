package edu.uchicago.gerber._07streams.E19_14;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class parallel {
    public static ArrayList<String> word_list = new ArrayList<>();
    public static void main(String[] args) throws IOException {
        try {
            File word_file = new File("/projava/src/main/java/edu/uchicago/gerber/_07streams/file.txt");
            Scanner word_reader = new Scanner(word_file);
            while (word_reader.hasNextLine()) {
                word_list.add(word_reader.nextLine().toLowerCase());
            }
            word_reader.close();
        } catch (FileNotFoundException e) {
            System.out.println("Cannot find the words.txt");
            return;
        }
//        System.out.println("Content of ArrayLiList:");
//        System.out.println(listOfLines);
//        Optional result = word_list.parallelStream().filter(w -> w.length()>=5).filter(w -> isPalindrome(w,0,w.length()-1) == true).findAny();
        Optional result = word_list.parallelStream().filter(w -> w.length()>=5).filter(w->w.equals(new StringBuilder(w).reverse().toString())).findAny();
        System.out.println(result);
    }
    public static boolean isPalindrome(String text, int start, int end) {
        if (start >= end) { return true; }
        else {
            char first = Character.toLowerCase(text.charAt(start));
            char last = Character.toLowerCase(text.charAt(end));
            if (Character.isLetter(first) && Character.isLetter(last)) {
                if (first == last) {
                    return isPalindrome(text, start + 1, end - 1);
                }
                else {
                    return false;
                }
            }
            else if (!Character.isLetter(last)) {
                return isPalindrome(text, start, end - 1);
            }
            else {
                return isPalindrome(text, start + 1, end);
            }
        }
    }
}
