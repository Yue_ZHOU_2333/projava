package edu.uchicago.gerber._07streams.E19_16;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
import java.util.OptionalDouble;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class groupbyTest {

    public static void main(String[] args) throws IOException {
        Map<Character, List<String>> characterListMap = groupByFirstChar("/Users/zhouyue/words.txt");
        for (Character key : characterListMap.keySet()){
            List<String> list = characterListMap.get(key);
            OptionalDouble a = list.stream().mapToInt(w -> w.length()).average();
            System.out.printf("%s = %s %n",key,a.toString());
        }
        System.out.println(characterListMap);
    }
    public static Map<Character, List<String>> groupByFirstChar(String fileName) throws IOException {
        Path PATH = Paths.get(fileName);
        return Files.lines(PATH).
                flatMap(s -> Stream.of(s.split("[^a-zA-Z]"))).
                filter(s -> s.length() > 0).
                map(s -> s.toLowerCase()).
                collect(Collectors.groupingBy(s -> s.charAt(0)));
    }
}
