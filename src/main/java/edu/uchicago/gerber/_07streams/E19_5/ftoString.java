package edu.uchicago.gerber._07streams.E19_5;

import java.util.stream.Stream;

public class ftoString {
    public static void main(String[] args) {
        Stream<String> stringStream = Stream.of("a", "b", "c");
        Stream<Integer> IntStream = Stream.of(1, 2, 3);
        System.out.println(toString(IntStream,2));
    }
    public static <T> String toString(Stream<T> stream, int n){

        // Convert IntStream to String
        Object[] stringArray = stream.toArray();
        String s = "";
        for(int i = 0; i < n - 1; i++){
            s = s + stringArray[i] + ",";
        }
        s = s + stringArray[n - 1];

        // Print the String
        return s;
    }
}
