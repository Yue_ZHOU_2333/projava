package edu.uchicago.gerber._07streams.E19_6;

import java.util.Currency;
public class currencies {
    public static void main(String[] args) {
        Currency.getAvailableCurrencies().stream().map(s -> s.getDisplayName()).sorted().forEach(s -> System.out.printf(s));
    }
}
