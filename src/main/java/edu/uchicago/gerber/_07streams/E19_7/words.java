package edu.uchicago.gerber._07streams.E19_7;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class words {
    public static void main(String[] args) {
        List<String> cities = Arrays.asList("New York","Tokyo","New Delhi");
        Stream<String> stream = listToStream(cities);
        Stream<String> stringStream = Stream.of("abbb", "bcccc", "cddd", "d", "eeee");
        List<Object> result = stream.filter(w -> w.length() > 2).map(w -> w.charAt(0) + "..." + w.charAt(w.length()-1)).collect(Collectors.toList());
        System.out.println(result);
    }
    private static <T> Stream<T> listToStream (List<T> list) {
        return list.stream();
    }
}
