package edu.uchicago.gerber._07streams.P13_3;

import java.util.Scanner;

public class phone {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter pin number: ");
        String num = scanner.nextLine();
        System.out.println();
        System.out.printf("The keypad encodings for %s are:%n", num);
        enumerateWords(num);
    }
    static char[] getKeysForButton(int n) {
        switch (n) {
            case 0:
                return new char[] { ' ' }; //returning only space
            case 1:
                return new char[] { '.' }; //returning only dot
            case 2:
                return new char[] { 'A', 'B', 'C' };
            case 3:
                return new char[] { 'D', 'E', 'F' };
            case 4:
                return new char[] { 'G', 'H', 'I' };
            case 5:
                return new char[] { 'J', 'K', 'L' };
            case 6:
                return new char[] { 'M', 'N', 'O' };
            case 7:
                return new char[] { 'P', 'Q', 'R', 'S' };
            case 8:
                return new char[] { 'T', 'U', 'V' };
            case 9:
                return new char[] { 'W', 'X', 'Y', 'Z' };
        }
        return null;
    }
    static void enumerateWords(String num) {
        if (num != null)
            enumerateWords(num, "");
    }

    static void enumerateWords(String num, String text) {
        if (num.length() == 0) {
            System.out.println(text);
        } else {
            int digit = num.charAt(0) - '0';
            char letters[] = getKeysForButton(digit);
            if (letters != null) {
                for (int i = 0; i < letters.length; i++) {
                    enumerateWords(num.substring(1), text + letters[i]);
                }
            }
        }
    }
}
