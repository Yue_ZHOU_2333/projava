package edu.uchicago.gerber._07streams.P13_3;

import java.util.*;
import java.io.*;

public class phonePad {
    public static void main(String[] args) {
        try {
            File word_file = new File("/projava/src/main/java/edu/uchicago/gerber/_07streams/words.txt");
            Scanner word_reader = new Scanner(word_file);
            while (word_reader.hasNextLine()) {
                word_dict.add(word_reader.nextLine().toLowerCase());
            }
            word_reader.close();
        } catch (FileNotFoundException e) {
            System.out.println("Cannot find the words.txt");
            return;
        }

        Scanner scanner = new Scanner(System.in);
        System.out.print("input the number: ");
        String number = scanner.nextLine();
        scanner.close();
        if (number.length() == 0) {
            System.out.println("Invalid input");
            return;
        }
        for (char c: number.toCharArray()) {
            if (c < '2' || c > '9') {
                System.out.println("Invalid input");
                return;
            }
        }
        recurGetWordSeq(number, 0, new ArrayList<String>(){{add("");}});
        System.out.println("output:");
        for (List<String> word_seq: possible_word_seqs) {
            for (int i = 0; i < word_seq.size(); i++) {
                System.out.printf(i == word_seq.size()-1 ? "%s\n" : "%s ", word_seq.get(i));
            }
        }
    }
    public static HashSet<String> word_dict = new HashSet<>();
    public static List<List<String>> possible_word_seqs = new ArrayList<>();
    public static boolean checkIfWord(String to_check) {
        if (to_check.length() == 0 || to_check.length() == 1) {
            return false;
        }
        return word_dict.contains(to_check);
    }
    public static void recurGetWordSeq(String number, int cur_index, List<String> cur_word_list) {
        if (cur_index == number.length()) {
            if (checkIfWord(cur_word_list.get(cur_word_list.size()-1))) {
                possible_word_seqs.add(new ArrayList<>(cur_word_list));
            }
            return;
        }

        char cur_number = number.charAt(cur_index);
        for (char possible_char: pin_code_map.get(cur_number)) {
            List<String> new_list = new ArrayList<>(cur_word_list);
            new_list.set(new_list.size()-1, new_list.get(new_list.size()-1) + possible_char);
            if (checkIfWord(new_list.get(new_list.size()-1))) {
                List<String> new_list_with_split = new ArrayList<>(new_list);
                new_list.add("");
                recurGetWordSeq(number, cur_index+1, new_list_with_split);
            }
            recurGetWordSeq(number, cur_index+1, new_list);
        }
    }

    public static HashMap<Character, char[]> pin_code_map = new HashMap<Character, char[]>(){{
        put('2', new char[]{'a', 'b', 'c'});
        put('3', new char[]{'d', 'e', 'f'});
        put('4', new char[]{'g', 'h', 'i'});
        put('5', new char[]{'j', 'k', 'l'});
        put('6', new char[]{'m', 'n', 'o'});
        put('7', new char[]{'p', 'q', 'r', 's'});
        put('8', new char[]{'t', 'u', 'v'});
        put('9', new char[]{'w', 'x', 'y', 'z'});
    }};
}
