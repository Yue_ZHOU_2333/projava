package edu.uchicago.gerber._07streams.YodaSpeakRecursive;

import java.util.Arrays;

public class YodaR {
    public static void main(String[] args) {
        String setence = "The force is strong with you";
        String[] strgs = setence.split(" ");
        System.out.println(Yoda(strgs));
    }
    static String Yoda(String[] strgs){
        if(strgs.length == 1){
            return strgs[0];
        }
        String word = strgs[0];
        String strs[] = Arrays.copyOfRange(strgs, 1, strgs.length);
        return Yoda(strs) + " " + word;
    }
}
